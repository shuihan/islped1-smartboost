% !TEX root =  ../islped17_SmartBoost.tex
\section{Evaluations}\label{sec:experiment}

The performance and power consumption using SmartBoost are evaluated in this section.

\subsection{Experimental Setup}
% HW perspective
We evaluate the SmartBoost based on the gem5\cite{gem5} simulation framework. The processor has 4 core, 4 private L2 caches and a shared L3 cache. Each of them is assigned a voltage domain to facilitate the power management.The detailed parameters can be found in Table~\ref{tab:config}.

% SW perspective
Targeting on shared-memory programs for multiprocessors, PARSEC~\cite{parsec} benchmark suite are executed (except `rtview' and `vips') in our evaluations. Benchmarks are abbreviated labeled by their first 4 characters in this section.
All benchmarks are detailed executed for 100 milliseconds after fast-forwarding the beginning segments, which is enough to evaluate the proposed power migration framework. The statistics and traces are collected periodically for profile and run-time analysis. The statistics from gem5 are analyzed by McPAT~\cite{mcpat} to generate the data traces of power consumption. The voltage information is obtained from VoltSpot~\cite{ISCA14-Runjie} by inputing the power traces. Both McPAT and VoltSpot are validated against the data of commercial processors, and are deliberately used in this work considering the model error cancel~\cite{Xi2015}.

\begin{table}[thb]
%\vspace{-5pt}
\caption{System Configuration}\label{tab:config}
%\scriptsize
\footnotesize
\centering
%\vspace{-0pt}
\begin{tabular}{ |c|c| }
	\hline
		&	\multicolumn{1}{c|}{Configurations}	\\	\hline
	Core 	&		4 cores, ARM A15 Out-of-Order, 1.0 GHz\\ \hline
	L1 I/D 		&	32KB, private, 2-way, 64B,  LRU, 4 MSHRs, 8 WBs	\\	\hline	
	L2    	&  2MB, Private, 4-way, 64B, LRU, 20 MSHRs, 8 WBs	\\	\hline
	L3  	& 16 MB, Shared, 16-way, 64B, LRU, 20 MSHRs, 16 WBs	\\ \hline
	Memory 		&	512MB, 	DDR3-1600, 12.8GB/s, avg. lat. 30ns  \\ \hline
    voltage 		&	0.75V-1.5V  \\ \hline
\end{tabular}

\end{table}

\subsection{Run-time Traces}

% \begin{figure*}[bt]
% \centering
% \includegraphics[width=0.95\textwidth]{./fig/profilings/profile_all_dec.jpg}
% \caption{The variation of activity.}
% \label{fig:dyn_variation}
% \end{figure*}%
% The length of period is a sensitive parameter: Long period eases the design of frequency change; while short period eases the detection of power vibration. The time duration period of profiling do affect the formation of programming phase. If the duration is too large, 10ms for example, the program lot of phase changes are covered by this large window. Since this latency is larger than the unit of disk IO, and the opportunities to identify different phases are glued and covered. The problem of OS controlled off-chip DVFS suffers from this problem. It generally requires several ms to response, when loss lot of opportunity. We fond the 100us is a good granularity to see the change.

%Since the SmartBoost leverages the unbalance of power consumption, it is essential to understand the program execution variations.

\begin{figure*}[thb]
\centering
\includegraphics[width=1\textwidth]{./fig/new/figureTrace_Decision.png}
\caption{The variation traces of activity and SmartBoost decisions}
\label{fig:figureTrace_Decision}
\end{figure*}%

As mentioned, programs generally have executing phases that show the unbalance of power consumption among different components.
The activity traces and the SmartBoost decision traces made on voltage of a representative benchmark (``dedup'') is shown in Figure~\ref{fig:figureTrace_Decision}. The symbol ``CoreN'' indicates the normalized activity for the $N^{th}$ core, while the L2\_N is the private L2 cache for the core. The activities of caches and cores are normalized to their maximum activities, obeying the activity definitions in the proposed framework. The traces include $1000$ equal-length periods, whose duration is $100\mu s$. Shown by the activity traces, the program has execution phases: During periods $250$ to $300$, only one core is active, but the caches are sometimes busy.
%
%\begin{figure}[thb]
%\centering
%\includegraphics[width=0.45\textwidth]{./fig/new/figureTrace.png}
%\caption{The variation traces of activity.}
%\label{fig:figureTrace}
%\end{figure}%
%
%\begin{figure}[thb]
%\centering
%\includegraphics[width=0.45\textwidth]{./fig/new/figureDecision.png}
%\caption{SmartBoost decisions.}
%\label{fig:figureDecision}
%\end{figure}%

Tracking the activity of all the components, SmartBoost power migration framework decides whether the cores will be speed up or the caches will be turned off or slowed down.
%The decision traces made on voltage are shown in Figure~\ref{fig:figureDecision}.
All voltage values are normalized to their maximum values.
The value `0' for a core indicates the core is working at its lowest frequency, while the `0' of a cache indicates that the cache has been turned off using power gating techniques.
During the $250$-$300$ region, only the core $2$ is very active, thus the other components are in low voltage modes, when the power is migrated to the core $2$ and the L3 cache. During the periods from $400$ to $450$, four cores are all active. The SmartBoost maintains the voltage of L3 cache at low levels, to migrate power to cores.
The voltage statuses of private L2 caches are consistent with their cores.
Thus, in order to maximize the performance of the entire processor, SmartBoost sets the caches usually at low-voltage level, migrating the extra power to cores.

\subsection{Performance and Power Consumption}

% Although the idea of SmartBoost could also used in single-core processor, however, power migration between multi-core is the main point of SmartBoost. The multi-core processor has a larger power migration space compared to a single-core processor, since the large L3 can also be leveraged to contribute power. People always want to achieve high performance in a low power consummation, However, sometimes people need a faster processing speed without counting the consummation.
% In order to meet these two need, we would focus on the performance improvement and energy efficiency ratio of the SmartBoost.

%The performance of single core is still critical to several programs, since they show low thread level parallelism capability.

The results shown in Figure~\ref{fig:multicore} estimate the speedup within a 4-core ARM processor. The performance is calculated by taking average of the time used by the four cores. All performance speedups are compared with a baseline processor without SmartBoost or DVFS. CacheOff indicates switching off L2 and L3 cache and providing the rest power to cores.
CacheOff has a nice performance improvement in some benchmark (``blackscholes'' and ``fluidanimate''), and have a $18.4\%$ performance improvement in average. However, it does not work well in 'streamcluster' because caches indeed provide lot of benefit in this case.

As we mentioned, SmartBoost includes prediction and optimization. If SmartBoost has $100\%$ accuracy rate in prediction, the optimization would give an outcome which is nearing the optimal solution. The ``SmartBest'' labels this optimal solution, while ``SmartReal'' labels the actual results. In some benchmark (``blackscholes'', ``facesim''), the ``SmartReal'' permits a very close approach to the ``SmartBest'', thanks to the high prediction accuracy. However, in benchmark ``freqmine'' or ``x264'', the gap between ``SmartBest'' and ``SmartReal'' still exists. In those case, the behaviors of the cores and caches are hard to predict through pure hardware method. According to the Figure~\ref{fig:predictAcc} in Section~\ref{sec:prediction}, the prediction miss in benchmark ``blackscholes'' is negligible and the predictor performs not well in benchmark ``x264'', too.
On average, the performance of 'SmartBest' can be improved by $26.6\%$ and the performance of ``SmartReal'' can be improved by $22.9\%$.

\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/figurePerfor2.png}
\caption{Performance improvement for a 4-core ARM processor.}
\label{fig:multicore}
\end{figure}%

To investigate the energy efficiency, we run the same amount of work in SmartBoost, DVFS, cache-off and the baselines.The energy consumption of cores and caches(not include SmartBoost itself) in different benchmarks is shown in Figure~\ref{fig:energy}.
DVFS does do well compared to the baselines, which shows a significant effective energy consummation for benchmark ``ferret'', whose processor is blocked by hard drive accesses for a long time. In this case, the DVFS is nearly as efficient as SmartBoost, since the entire processor is blocked and idle.
For the benchmarks (e.g. freqmine) that only the caches are also active, DVFS does worse than SmartBoost, since the voltage scaling of DVFS is processor-level.
On the other hand,
%there is a noticeable comparation between cache-off and SmartBoost.
although cache-off always reduces the energy cost, SmartBoost does even better. The cache-off reduces the power of chip by reducing the voltage of cache, however it will increase the run time, thus consume more energy.
In average, the energy consumption of SmartBoost is $25.4\%$ less than normal case.

\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/EnergyConsumption.png}
\caption{Energy consumption for a 4-core ARM processor.}
\label{fig:energy}
\end{figure}%

We also compare the average power of whole chip(inlclude SmartBoost itself) shown in Figure~\ref{fig:power}. In average, the power efficiency of SmartBoost with ILP is $19.5\%$ higher than normal case. It has a $10.4\%$ efficiency improvement than DVFS. The SmartBoost with table uses $5.3\%$ power less than SmartBoost with ILP.
%According to the Table~\ref{tab:hardwareOverhead} in Section~\ref{sec:design}, the power of SmartBoost is less than $0.14W$. In every $100us$, the SmartBoost would active no more than $10us$. Thus, its average power is $0.5\%$ of total power of chip with SmartBoost.

\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/exprPower2.png}
\caption{Average power for a 4-core ARM processor.}
\label{fig:power}
\end{figure}%

\subsection{Comparision}

The Comparison between SmartBoost and other power migration methods is shown in Table~\ref{tab:summerizeSmartBoost}.

\begin{table}[thb]
%\vspace{-5pt}
\caption{Comparison}\label{tab:summerizeSmartBoost}
%\algsetup{linenosize=\tiny}
\scriptsize
%\footnotesize
\centering
\begin{tabular}{|c|c|c|}
\hline
                    & advantage                 & disadvantage \\ \hline
\multirow{2}{*}{SmartBoost(Table)}   & high performance; less             & limited number of components \\
                                     &   time overhead; less power          &      in chip;     \\ \hline
\multirow{2}{*}{SmartBoost(ILP)}     & \multirow{2}{*}{high performance}          & higher design difficulty; \\
                                     &                                         &   \\ \hline
\multirow{2}{*}{standard DVFS}       & \multirow{2}{*}{reliable}                  & do worse in power shortage; \\
                                     &                           & could not optimise caches  \\ \hline
\multirow{2}{*}{Cache-Off}       & \multirow{2}{*}{easy to design}            & do worse in data-intensive  \\
                                     &                           & program  \\ \hline
%                      & SmartBoost(Table)   &SmartBoost(ILP)   &standard DVFS   &Cache-Off             \\\hline
%          advantage   & less overhead       & high performance &reliable        &easy to design      \\    \hline
%     disadvantage     & number of core and cache is limited & higher hardware overhead & could not optimise cache & do worse in data-intensive program \\ \hline
\end{tabular}
\end{table}
