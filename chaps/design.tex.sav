% !TEX root =  ../islped17_SmartBoost.tex
\section{Design of SmartBoost}\label{sec:design}

SmartBoost optimizes the performance limited by power according to the projection of activity, depending on solving integer linear programming problem at runtime. Different from standard DVFS, caches are also considered as individual components in the power management.
This section introduces the design of activity predictor, ILP model, power migration policy and also analyzes the overhead of our designs.

\subsection{Projection of Activity}\label{sec:prediction}

The activity is quantified to facilitate the power management. It indicates the activity level of a component of chip in a time period.
The core activity is defined as the actual instruction amount per sample period normalized with its maximum value; while the cache activity uses the access amount per sample period.
The activity of the processor can be represented as an activity vector $\bm{A} = (a_0, a_1, ..., a_n)$, where $a_i \in [0,1]$.

The activity history is used to predict the activity vector in next sample period. To reduce the time overhead of the predictor, we predict the activity of next period in parallel with processor operations of current period.
We propose two prediction methods: polynomial prediction and moving average prediction. Polynomial prediction fits a linear/quadratic/cubic curve, using two/three/four history records. Using the curve, the next record can be predicted.
The second method calculates the exponential moving average(EMA) of the history activity. The recent record is assigned a doubled weight than the its previous one, which is shown in Equation (\ref{eq:Weighting}).

\begin{equation}\label{eq:Weighting}
%\scriptsize
\begin{aligned}
\bm{A}_n = \frac{2^{k-1}\times\bm{A}_{n-1}+2^{k-2}\times\bm{A}_{n-2}+...1\times\bm{A}_{n-k}}{2^{k-1}+2^{k-2}+...1}
\end{aligned}
\end{equation}

The prediction accuracy of those methods is tested by PARSEC benchmark in Table~\ref{tab:predict}. The relative differences between the ``real'' and the predicted activity value for all predicted records are averaged to calculate the prediction accuracy.
$History Record$ indicates how many history activity records are used by that prediction method. The quadratic polynomial prediction needs at least $3$ points to determine a quadratic curve; and the cubic polynomial needs $4$ records. The `-' sign means more records are necessary for that prediction method.

\label{rewrite:tab-predict}{rewrite:tab-predict}

\begin{table}[thb]
%\vspace{-5pt}
\caption{Prediction accuracy of different methods}
\label{tab:predict}
%\scriptsize
\footnotesize
\centering
\begin{tabular}{|c|c|c|c|}

\hline
                        & \multicolumn{3}{c|}{History Record}   \\  \hline
Prediction            &{2}             &{3}         &{4}       \\	\hline
{EMA }                          & $69.14\%$      &$62.26\%$    &$55.32\%$           \\	\hline
{Linear Poly }                         & $\mathbf{71.02\%}$      &$65.65\%$    &$66.74\%$   \\   \hline
{Quadratic Poly }                      & -             &$60.29\%$    &$61.84\%$  \\	\hline
{Cubic Poly }                          & -             &-           &$57.07\%$     \\	\hline
\end{tabular}
\end{table}

The linear polynomial using $2$ history activity records shows the best prediction accuracy. It is interesting to see that higher level of polynomial prediction does not show higher accuracy. This is because the sudden variation of the activity shows very little predictability if no operating system support is provided. Thus we choose the linear polynomial prediction method in SmartBoost. The average prediction accuracy is $66\%$, shown in the Figure~\ref{fig:predictAcc}. This accuracy maybe improve if SmartBoost has operation system support.

\label{rewrite:fig-predictACC}{rewrite:fig-predictACC}

 \begin{figure}[thb]
 \centering
 \includegraphics[width=0.45\textwidth]{./fig/new/predictAcc.png}
 \caption{Prediction accuracy of different benchmarks using linear fit prediction}
 \label{fig:predictAcc}
 \end{figure}%

%The activity history is used to predict the activity vector in next sample period.
%The activity vector at the $n_{th}$ period ($\bm{A_n}$) is predicted according to its previous two periods($\bm{A_{n-2}}$ and $\bm{A_{n-1}}$). We explore the relationship among them by adjusting the degree of the polynomial fitting functions. We select linear relation, since it shows good enough performance and consumes the few cost at run-time.

% $1_{th}$ to $i_{th}$ time segment, then predictor of SmartBoost would give us a forecast of activity level in $(i+1)_{th}$ time segment, which just bases on the history record. To achieve this goal, we try to use least squares polynomial curve fitting. For example, if we have three points in a plane, we could find a quadratic curve in the plane to minimize the sum of the distant between the points and the curve, such as Figure~\ref{fig:lsmPredict}. According to the principle of least square, we have reason to believe the fourth point would fall nicely on this curve.

% \begin{figure}[thb]
% \centering
% \includegraphics[width=0.45\textwidth]{./fig/new/lsmPredict.png}
% \caption{Prediction of the fourth point.}
% \label{fig:lsmPredict}
% \end{figure}%

% The Smart Boost would project the activity level of components and try to distribute power for next time segment depending on the projection. So the accuracy of projection is a very important thing which is highly related the performance of the Smart Boost. Then we need an indication of how our predictor works.

%The accuracy of the prediction is crucial to the efficiency of the power management.
%We profile the ``real'' activity through all the sample periods offline, and labels them as $a_{real}$. The absolute difference between the ``real'' and predicted activity ($a_{real} - a_{project}$)is used to indicate the prediction error. of $(a_{real} - a_{project})$ would reflect the expect deviation of activity projection. In order to summarize the values, we define deviation as the average of these error, described by the Equation (\ref{eq:DeviationEquation}).
%
%\begin{equation}\label{eq:DeviationEquation}
%%\scriptsize
%\begin{aligned}
%Deviation = \frac{\sum_{i=1}^{n} |Activity_{Real}.a_i - Activity_{Project}.a_i|}{n}
%\end{aligned}
%\end{equation}
%
%A no-projection baseline is used to show the efficiency of the prediction. The no-projection version always believes the activity is $0.5$. We show the prediction accuracy of both in Figure~\ref{fig:Deviation}. Our prediction for some benchmark (e.g. 'blakscholes') is very good ($xx$). However, some benchmarks (e.g. 'x264') has poor performance due to frequency significant variation.
%
%\begin{figure}[thb]
%\centering
%\includegraphics[width=0.45\textwidth]{./fig/new/Deviation.png}
%\caption{The prediction accuracy of no-projection and projection.}
%\label{fig:Deviation}
%\end{figure}%

% The main problem we are facing in this case is that we cannot always assure the behaviors of the core or cache would fit a polynomial curve. Even just using more historical data could not ensure the accuracy rate could be improved. Sometimes, the activity of cores is abrupt and erratic. To reduce the deviation, we would adjust the degree of the fitting polynomial curve. In this situation, linear polynomial fitting does better than quadratic or cubic polynomial fitting. Meanwhile, a lower fitting polynomial degree also consume less power.

\subsection{Integer Linear Programming}

% The features of a system, such as performance, power consumption, and reliability, are usually categorized as two-fold. One is constrictive, limiting certain value within constraints. The others are optimization driven, requiring certain values to be maximized or minimized. If the power consumption is the optimization target, as more power as possible is needed to remove from the chip, as long as the negative performance impact is within the constraint. This is the typical target of conventional DVFS designs. Within this work the optimization target is performance, while the power is taken as the constraint.
On this section, we focus on how the ILP works. We need voltage contribution of $n$ major components(cores and caches), thus n-length voltage vector $\bm{V}$ is the variables of ILP. The main constraint of our ILP is a power limitation, and the objective function is performance gain. Another n-length vector $\bm{A}$ is used to represent the activity of components within the processor, which help SmartBoost to set the constraints and objective function in run time.
To implement an efficient run-time power migration policy, the value of $\bm{A}$ and $\bm{V}$ is discretized to five and three levels, respectively.

The power cost constraint equation is represented as Equation~\ref{eq:powerConstraint}. $P_{sepc}$ is the power limitation, $P_{cost}$ is the power cost we evaluated. ILP need a linear function about voltage, thus a linear fitting between voltage ($\bm{V}$) and power ($P_{cost}$) is necessary.

Based on previous work~\cite{de2014energy}, the core power is represented by the Equation~(\ref{eq:totalPower}). The $\gamma$ is a temperature dependent constant. $\alpha$ is the activity factor. $\eta$ is a scaling factor representing the effect of short-circuit power. $C$ is the capacitance of the processor, the $V$ is the supply voltage, and the $f$ is the frequency.

\begin{equation}\label{eq:totalPower}
%\scriptsize
P_{cost} = P_{dynamic} + P_{leakage} = (1+\gamma V)\eta \alpha C f V^2
\end{equation}

Actually, the relationship between the maximum switching frequency and supply voltage is fitted by a linear function in Figure~\ref{fig:Volfit}. The fitting coefficients are $388$ and $738$, respectively. Within the fitting period of voltage from $0.75V$ to $1.5V$, the R square is $0.99$, which indicates a good fit. According to this fitting model, an ARM core working at 1V can run at about 1.1GHz.

\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/frequency-voltage-fit.png}
\caption{Scatter plot and fitting curve between frequency and voltage.}
\label{fig:Volfit}
\end{figure}%

Thus, the $f$ in Equation~(\ref{eq:totalPower}) could be considered as a linear term of $V$.

The $k_{p}$ and $b_{p}$ is first-order and constant coefficient of linear fitting between voltage and power, respectively. Note that the relation between voltage and power is simplified to the linear relationship, to facilitate efficient run-time computation. The detailed error analysis is shown in Section \ref{sec:model-regression}. We define $\bm{K}_{p}$ to indicate $k_{p}\cdot\bm{V}$ and ${B}_{p}$ to indicate $\bm{A}_T(b_{p}\cdot\bm{I})$ for ILP computation.

\begin{equation}\label{eq:powerConstraint}
%\scriptsize
P_{spec} \geq P_{cost} = \bm{V}^T(k_{p}\cdot\bm{A}) + \bm{A}_T(b_{p}\cdot\bm{I}) = \bm{K_p}^T\cdot\bm{A} + B_p
\end{equation}

SmartBoost uses gain (reduction of execution time) to indicate the performance. The performance vector ($\bm{G}$) consists of the gain of cores ($g_{core}$) and caches ($g_{cache}$). We want to know which component of chip need more power, thus it is a priority issues and does not need exorbitant precision. We try to grasp the principal gain and omit the influence of interact between caches and caches. A different different clock rates between core and its private cache could reduce data transfer efficiencies. However, this efficiency loss is ignorable and is same for all cores and caches, thus we just overlook it.

The gain of a core ($g_{core}$) is based on the time consumed in the core pipeline, which is linear related to the core frequency. The gain is calculated by Equation~(\ref{eq:coregain}), where the $t_{inst}$ is the average time consumed by each instruction, and $f_o, f$ are the core frequencies before and after the voltage change, respectively. In the second line of the equation, the $N_{inst}$ is expressed as $\alpha \cdot Activity$, the value of $\alpha$ should be the instruction number of the core whose activity is $1$. The $g_{core}$ is expanded in a Taylor series and we choose the linear terms of $V$ to get an approximation of the relation between the $g_{core}$ and $V$. We set the $V_o$ as $1.1V$ in our evaluation platform. In this case, the deviation of our fitting is less than $0.3\%$ when the voltage is in range $0.75V$ to $1.5V$.

\begin{equation}\label{eq:coregain}
%\scriptsize
\begin{aligned}
g_{core} &= N_{inst} \cdot t_{inst} \cdot f_o \cdot (\frac{1}{f_o} - \frac{1}{f}) \\
 		 &\approx \alpha \cdot Activity \cdot t_{inst} \cdot \frac{k(V - V_o)}{f_o}
\end{aligned}
\end{equation}

% \begin{equation}\label{eq:coregain}
% %\scriptsize
% \begin{aligned}
% g_{core} &= N_{instruction} \cdot t_{instruction} \cdot f_o \cdot (\frac{1}{f_o} - \frac{1}{f}) \\
%          &= N_{instruction} \cdot t_{instruction} \cdot (1 - \frac{f_o}{f_o + k(V - V_o)})  \\
%          &= N_{instruction} \cdot t_{instruction} \cdot (1 - \frac{1}{1 + \frac{k(V - V_o)}{f_o}}) \\
%          &\approx N_{instruction} \cdot t_{instruction} \cdot (1 - (1 - \frac{k(V - V_o)}{f_o})) \\
%          &= N_{instruction} \cdot t_{instruction} \cdot \frac{k(V - V_o)}{f_o}
% \end{aligned}
% \end{equation}



% \begin{figure}[thb]
% \centering
% \includegraphics[width=0.45\textwidth]{./fig/new/coreGain.png}
% \caption{The fitting line between Voltage and core Gain}
% \label{fig:coreGain}
% \end{figure}%

The gain achieved by caches ($g_{cache}$) comes from the reduction of cache misses. To increase the accuracy of approximation, only L2 and L3 caches will be turned off or slowed down, which will not stall the memory stage of pipeline due to fetch miss during power off. Previous work \cite{Sundararajan-HPCA'12, Gaur:2011:bypass, Zhang-ISLPED'14} shows that the L2/L3 access latency (including hit and miss) can only be reflected to the overall execution time by a factor. Thus the performance gain of turning on a cache can be estimated by Equation~(\ref{eq:cachegain}), where the $\beta$ is the constant used to represent the core's cache latency cover effect. The overhead due to loss of data should also be considered. Dirty data within the cache may be lost, if the cache is shutdown entirely. Thus, we use eager data write back policies and non-inclusive cache hierarchy to keep the data integrity. In this way, the potential data coherency issue is solved. But the overhead of loading data back also reduces the gain. Considering all the overhead, we set the $\beta$ to $0.1$ in our model.

\begin{equation}\label{eq:cachegain}
%\scriptsize
\begin{aligned}
g_{cache}  &= \beta \cdot N_{access} \cdot (t_{miss} - t_{hit}) \\
               &\approx \beta \cdot \alpha \cdot Activity \cdot period \cdot \frac{k(V - V_o)}{f_o^2}
\end{aligned}
\end{equation}

The $t_{miss} - t_{hit}$ is also inversely proportional to $f$ which has a linear relationship with $V$. Thus, we also could use a Taylor series to find approximate linear relationship between $g_{cache}$ and $V$. The constant period indicates the time span of a period. We also expresses the $N_{access}$ as $\alpha \cdot Activity$. We set $V_o$ as $1.1V$. In this case, the deviation of our fitting is less than $1\%$ when the voltage($V$) is in range $0.75V$ to $1.5V$.

Based on above simplification, we convert the power management problem to an integer linear programming (ILP) problem. The power limitation is described by the Equation~(\ref{eq:restriction}). And the optimization function is the gain of the processor, described by the Equation (\ref{eq:optfunc}). The $\bm{V}_{core}$ and $\bm{V}_{cache}$ are the core part and cache part of the $\bm{V}$. The $B_{g}$ is the sum of the ${B}_{gCore}$ and ${B}_{gCache}$. The ${B}_{gCore}$ indicates $\bm{A}_{core}^T(b_{gCore}\cdot\bm{I})$, and the ${B}_{gCache}$ indicates $\bm{A}_{cache}^T(b_{gCache}\cdot\bm{I})$. The $\bm{K}_{gCore}$ indicates the $k_{gCore}\cdot\bm{A}_{core}$, and the $\bm{K}_{gCache}$ indicates the $k_{gCache}\cdot\bm{A}_{cache}$. Those coefficient ($k_{gCore}$, $k_{gCache}$, $b_{gCore}$, $b_{gCache}$) are the fitting results of the expanded Tyler series of Equation~(\ref{eq:coregain}) and (\ref{eq:cachegain}). The ${B}_{gCache}$ is the overall constant coefficient summarizing all components' constant fitting coefficients.

\begin{equation}\label{eq:restriction}
P_{spec} \geq P_{cost} = \bm{V}^T\bm{K}_p + {B}_p
\end{equation}
\begin{equation}\label{eq:optfunc}
G_{perf} = \bm{V}_{core}^T\bm{K}_{gCore}+ \bm{V}_{cache}^T\bm{K}_{gCache} + {B}_{g}
\end{equation}


\subsection{SmartBoost Power Migration}

Using an ILP solver, the SmartBoost power migration is implemented by the Algorithm~\ref{ag:ILP}. The SmartBoost work in parallel with the other components of the chip.
The algorithm can be divided into two parts.
(a) Line 1-2: The coefficients are initialized in factory, according to the configuration and attributes of the processor.
(b) Line 3-18: At the end of each sampling period, the activity of all components are predicted according to their history, and the $\bm{K}_{p}$, $B_p$, $\bm{K}_{gCore}$, $\bm{K}_{gCache}$ and $B_{g}$ are calculated individually. At last the $\bm{V}$ is produced by the ILP solver.

\begin{algorithm}
\caption{SmartBoost Power Migration Algorithm}
\label{ag:ILP}
\begin{algorithmic}[1]
\STATE Initialize $\bm{K},\bm{B}$
\STATE $\bm{A_0} \leftarrow 1, \bm{V} \leftarrow 1.$
\FOR {each sampling period $k$}
    \STATE Initialize ${B}_{p}$, ${B}_{gCore}$, ${B}_{gCache}$ to $0$.
    \STATE $\bm{A}_{k+1} \leftarrow $ActivityPredictor($\bm{A}_{k-1}, \bm{A}_{k}$)
    \STATE $\bm{K}_{p} \leftarrow k_{p}\cdot\bm{V}$
	\STATE $\bm{K}_{g} \leftarrow k_{g}\cdot\bm{V}$
	\FOR {each component $i$}
        \IF {component $i$ is a core}
            \STATE $\bm{K}_{gCore} \leftarrow k_{gCore}\cdot\bm{A}_{k+1}[i]$
            \STATE ${B}_{gCore} \leftarrow {B}_{gCore} + b_{gCore}\cdot\bm{A}_{k+1}[i]$
        % \ENDIF
        \ELSIF {component $i$ is a cache}
            \STATE $\bm{K}_{gCache} \leftarrow k_{gCache}\cdot\bm{A}_{k+1}[i]$
            \STATE ${B}_{gCache} \leftarrow {B}_{gCache} + b_{gCache}\cdot\bm{A}_{k+1}[i]$
        \ENDIF
	\ENDFOR
    \STATE ${B}_{g} \leftarrow {B}_{gCore} + {B}_{gCache}$
	\STATE $\bm{V} \leftarrow $ILPsolver($\bm{K}_{p}$, $\bm{K}_{gCore}$, $\bm{K}_{gCache}$, ${B}_{p}$, ${B}_{g}$)
\ENDFOR
\end{algorithmic}
\end{algorithm}

% The ActivityPredictor which is based on polynomial fitting method ends up using linear fitting method in order to reduce the computation complexity. Actually, leaner fitting needs at least $2$ history $\bm{A}$. In the algorithm mentioned above, we use $2$ history record($\bm{A^{k}},\bm{A^{k-1}}$), because it has better performance. When the predictor facing jumping activity, lesser history record means a quicker response.

\subsection{Design Overhead}

To reduce overhead of activity predictor, we use the linear polynomial prediction method. According to the design compiler, the predictor of one component of the chip is not larger than $2000$ transistors. For the 4-core ARM chip used in our evaluation, it is divided into 9 components: 4 cores (includes L1 I/D caches), 4 private L2 caches and one shared L3 cache. The total hardware overhead of the predictor is less than $20\times10^{3}$ transistors. For each predictor of the component, we need a $2$-word space to record the history activity level, thus we need $18\times64 bit = 1152 b$ space.

In order to make the ILP solver fast enough for runtime decision making, we employ a hardware ILP solver proposed by previous work \cite{simplex-hardware}.
The scale of the ILP problem is estimated by the product of the number of variables and the number of constraint equations. The voltage vector is $9$ units in length. And the upper and lower bounds of the voltage contribute to $18$ constraints. In short, the ILP problem of a 4-core processor has a $9\times19$ scale. We use $2\times2$ block size and $12-bit$ word-length ILP hardware design proposed in previous work \cite{simplex-hardware}. According to the design compiler, the hardware overhead of ILP is about $2556\times10^{3}$ transistors.
There are a $18-Kbit$ RAM in each block, thus the ILP hardware solver need $72-Kbit$ RAM. The time cost of the ILP solver is less than $10us$.

% The latency which is caused by the computation of SmartBoost means the interval between periods of program. If we want divide the program into some $100us$ time segments, the run time of SmartBoost could not too long to reduce the performance of the chip. The main part of run time of SmartBoost is ILP. The good news is that the ILP hardware could get result quickly just like some example in \cite{simplex-hardware}. The scale of the ILP problem could be estimated by the product of the number of variables and the number of constraint equations. As we mentioned before, the problem we faced is only $9\times19$. Some more complex problems could solved in less than $10us$, which means that our design is practical.

Evaluated at 22nm CMOS processing node, the detailed overhead analysis is shown in Table~\ref{tab:hardwareOverhead}. The span of period is $100us$. The overall time needed of SmartBoost is less than $10us$, thus SmartBoost could give the chip a voltage distribution of next period in time.

\begin{table}[thb]
%\vspace{-5pt}
\caption{The overhead of SmarBoost}\label{tab:hardwareOverhead}
%\algsetup{linenosize=\tiny}
\scriptsize
%\footnotesize
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
    & Transistor       &Memory&Area         &Power   & Time         \\
    &                  & (Kb) & (${mm}^2$)  & (mW)   &  (us)        \\    \hline
Predictor & $20\times10^{3}$    &$<$ $2$  &     $0.0025$   &$1.1$ &negligible           \\	\hline
ILP Solver  & $2556\times10^{3}$  & $72$           &     $0.3232$   &$136.9$ & $<$ 10       \\   \hline
Overall   & $2576\times10^{3}$  & $<$ $74$ &     $0.3257$  &$138.0$  & $<$ 10       \\	\hline
\end{tabular}
\end{table}

