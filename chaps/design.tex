% !TEX root =  ../islped17_SmartBoost.tex
\section{Design of SmartBoost}\label{sec:design}

SmartBoost optimizes the performance limited by power according to the projection of activity, depending on solving integer linear programming problem at runtime. Different from standard DVFS, caches are also considered as individual components in the power management.
This section introduces the design of activity predictor, ILP model, optimization, power migration policy and the overhead of our designs.

\subsection{Projection of Activity}\label{sec:prediction}

The activity is quantified to facilitate the power management. It indicates the activity level of a component of chip in a time period.
The core activity is defined as the actual instruction amount per sample period normalized with its maximum value; while the cache activity uses the access amount per sample period.
The activity of the processor can be represented as an activity vector $\bm{A} = (a_0, a_1, ..., a_n)$, where $a_i \in [0,1]$.

The activity history is used to predict the activity vector in next sample period.
We propose two prediction methods: polynomial prediction and moving average prediction. Polynomial prediction fits a linear/quadratic/cubic curve, using two/three/four history records. Using the curve, the next record can be predicted.
The second method calculates the exponential moving average(EMA) of the history activity. The recent record is assigned a doubled weight than the its previous one, which is shown in Equation (\ref{eq:Weighting}).
\begin{equation}\label{eq:Weighting}
%\scriptsize
\begin{aligned}
\bm{A}_n = \frac{2^{k-1}\times\bm{A}_{n-1}+2^{k-2}\times\bm{A}_{n-2}+...1\times\bm{A}_{n-k}}{2^{k-1}+2^{k-2}+...1}
\end{aligned}
\end{equation}
The prediction accuracy of those methods is tested by PARSEC benchmark in Table~\ref{tab:predict}. The relative differences between the ``real'' and the predicted activity value for all predicted records are averaged to calculate the prediction accuracy.
$History Record$ indicates how many history activity records are used by that prediction method. The quadratic polynomial prediction needs at least $3$ points to determine a quadratic curve; and the cubic polynomial needs $4$ records. The `-' sign means more records are necessary for that prediction method.
\begin{table}[thb]
%\vspace{-5pt}
\caption{Prediction accuracy of different methods}
\label{tab:predict}
%\scriptsize
\footnotesize
\centering
\begin{tabular}{|c|c|c|c|}

\hline
                        & \multicolumn{3}{c|}{History Record}   \\  \hline
Prediction            &{2}             &{3}         &{4}       \\	\hline
{EMA }                          & $77.70\%$      &$77.75\%$    &$77.50\%$           \\	\hline
{Linear Poly }                         & $\mathbf{79.26\%}$      &$78.66\%$    &$77.82\%$   \\   \hline
{Quadratic Poly }                      & -             &$67.35\%$    &$69.65\%$  \\	\hline
{Cubic Poly }                          & -             &-           &$61.76\%$     \\	\hline
\end{tabular}
\end{table}
The linear polynomial using $2$ history activity records shows the best prediction accuracy. Higher level of polynomial prediction does not show higher accuracy, because the sudden variation of the activity shows very little predictability if no operating system support is provided. Thus the linear polynomial prediction method is chosen in SmartBoost. The average prediction accuracy is $79.26\%$, shown in the Figure~\ref{fig:predictAcc}.
 \begin{figure}[thb]
 \centering
 \includegraphics[width=0.45\textwidth]{./fig/new/predictAcc.png}
 \caption{Prediction accuracy of different benchmarks using linear fit prediction}
 \label{fig:predictAcc}
 \end{figure}
\subsection{Integer Linear Programming}

On this section, we focus on how the ILP works. Voltage contribution of $n$ major components(cores and caches) is outcome of ILP, thus n-length voltage vector $\bm{V}$ is the variables. The main constraint of ILP is power limitation, and the objective function is performance gain.
Another n-length vector $\bm{A}$ is used to represent the activity of components within the processor, which is mentioned before.
To implement an efficient run-time power migration policy, the value of $\bm{A}$ and $\bm{V}$ is discretized to five and three levels, respectively.

\
\subsubsection{Power Constraint Equation}
\

The power cost constraint equation is represented as Equation~\ref{eq:powerConstraint}. $P_{sepc}$ is the power limitation, $P_{cost}$ is the power cost we evaluated.
\begin{equation}\label{eq:powerConstraint}
%\scriptsize
P_{spec} \geq P_{cost}
\end{equation}
Linear fitting between voltage ($\bm{V}$) and power ($P_{cost}$) for ILP constraint is shown in Figure~\ref{fig:powerfit}.
The data are based on a previous technical report~\cite{Vogeleer-14}. Since the data reported are based on $0.18\mu m$ technology node, we scale it to $45nm$ node ARM processor to make them compatible with other data collected from CACTI~\cite{cacti}. Within in the $[750,1500]mV$ voltage range, the relation can be approximated by a linear function without losing too much accuracy.
\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/power-voltage-fit.png}
\caption{Scatter plot and fitting curve between power and voltage.}
\label{fig:powerfit}
\end{figure}
\begin{equation}\label{eq:powerConstraint2}
%\scriptsize
P_{spec} \geq P_{one} = V \cdot K_{one} + B_{one}
\end{equation}
Power constraint equation of one component is represented as Equation~\ref{eq:powerConstraint2}. The $P_{one}$ indicates power of one component. The $K_{one}$ and $B_{one}$ is first-order and constant coefficient of linear fitting between voltage and power, respectively.

With different activity level, the power of component would change in equal proportion.
For runtime, power cost constraint equation of all components with different activity is shown as Equation~\ref{eq:powerConstraint3}.
The $\bm{I}$ is the n-dimensional unit vector.
To make the linear relationship between voltage and power more clear, the $\bm{K}_{p}$ is defined to indicate $K_{one}\cdot\bm{A}$.
\begin{equation}\label{eq:powerConstraint3}
%\scriptsize
\begin{aligned}
P_{spec} &\geq \bm{V}^T(K_{one}\cdot\bm{A}) + \bm{A}^T(B_{one}\cdot\bm{I}) \\
         &= \bm{V}^T\cdot\bm{K_p} +\bm{A}^T(B_{one}\cdot\bm{I})
\end{aligned}
\end{equation}

\
\subsubsection{Performance Objective Function}
\

The objective function is used to describe the target of the ILP. SmartBoost uses gain (reduction of execution time) to indicate the performance. The performance gain consists of the gain of cores ($Q$) and caches ($H$), which is shown in Equation~\ref{eq:gainObjectiveFunction}. The influence of interact between caches and caches is omitted here. Which component of chip need more power is a priority issues and does not need exorbitant precision.
\begin{equation}\label{eq:gainObjectiveFunction}
%\scriptsize
G = Q + H
\end{equation}
The gain of a core ($Q_{one}$) is based on the time consumed in the core pipeline, which is linear related to the core frequency. The gain is calculated by Equation~(\ref{eq:coreGain}), where the $t_{inst}$ is the average time consumed by each instruction, and $f_o, f$ are the core frequencies before and after the voltage change, respectively.
\begin{equation}\label{eq:coreGain}
%\scriptsize
\begin{aligned}
Q_{one} &= N_{inst} \cdot t_{inst}  \cdot f_o \cdot (\frac{1}{f_o} - \frac{1}{f}) \\
% 		 &\approx  \alpha \cdot Activity \cdot t_{inst} \cdot \frac{K_{f}(V - V_o)}{f_o}
\end{aligned}
\end{equation}
Actually, the relationship between the maximum switching frequency and supply voltage is fitted by a linear function in Figure~\ref{fig:Volfit}.Within the fitting period of voltage from $0.75V$ to $1.5V$, the R square is $0.99$. According to this fitting model, an ARM core working at 1V can run at about 1.1GHz. The relationship between frequency and voltage is shown in Equation~(\ref{eq:fAndV}).
\begin{figure}[thb]
\centering
\includegraphics[width=0.45\textwidth]{./fig/new/frequency-voltage-fit.png}
\caption{Scatter plot and fitting curve between frequency and voltage.}
\label{fig:Volfit}
\end{figure}%
\begin{equation}\label{eq:fAndV}
%\scriptsize
f = V \cdot K_{f} + B_{f};
\end{equation}
\begin{equation}\label{eq:coreGain1}
%\scriptsize
\begin{aligned}
Q_{one} &\approx  \alpha \cdot Activity \cdot t_{inst} \cdot \frac{K_{f}(V - V_o)}{f_o}
\end{aligned}
\end{equation}
To get a linear relationship, the $Q_{one}$ is expanded in a Taylor series and we choose the linear terms of $V$ in the second line of the Equation~(\ref{eq:coreGain1}).
The $N_{inst}$ is expressed as $\alpha \cdot Activity$, the value of $\alpha$ should be the instruction number of the core whose activity is $1$. The $K_{f}$ has mentioned in Equation~(\ref{eq:fAndV}).
The $V_o$ is set as $1.1V$ in our evaluation platform. In this case, the deviation of our fitting is less than $0.3\%$ when the voltage is in range $0.75V$ to $1.5V$.

Performance gain of all cores with different activity is shown in Equation~(\ref{eq:coreGain2}). The $\bm{A}_{core}$ is cores part of Activity vector ($\bm{A}$). The $\bm{I}$ also represent the unit vector. To make the linear relationship more clear, the $\bm{K}_{core}$ is defined to indicate $(\alpha \cdot t_{inst} \cdot K_{f})/(f_o) \cdot \bm{A}_{core}$. %and ${B}_{gCore}$ to indicate $\bm{K}_{gCore} \cdot (- V_o \cdot \bm{I})$.
\begin{equation}\label{eq:coreGain2}
%\scriptsize
\begin{aligned}
Q &= \frac{\alpha \cdot t_{inst} \cdot K_{f}}{f_o} \cdot \bm{A}_{core} \cdot (\bm{V}_{core} - V_o \cdot \bm{I}) \\
         &= \bm{K}_{core} \cdot \bm{V}_{core} + \bm{K}_{core} \cdot (- V_o \cdot \bm{I})\\
%         &= \bm{K}_{gCore} \cdot \bm{V}_{core} + B_{gCore} \\
\end{aligned}
\end{equation}
The gain achieved by caches ($H_{one}$) comes from the reduction of cache misses. To increase the accuracy of approximation, only L2 and L3 caches will be turned off or slowed down, which will not stall the memory stage of pipeline due to fetch miss during power off. Previous work \cite{Sundararajan-HPCA'12, Gaur:2011:bypass, Zhang-ISLPED'14} shows that the L2/L3 access latency (including hit and miss) can only be reflected to the overall execution time by a factor. Thus the performance gain of turning on a cache can be estimated by Equation~(\ref{eq:cacheGain}).
The $\beta$ is the constant used to represent the core's cache latency cover effect and the overhead due to loss of data. Dirty data within the cache may be lost, if the cache is shutdown entirely. Thus, eager data write back policies and non-inclusive cache hierarchy is used to keep the data integrity. In this way, the potential data coherency issue is solved. But the overhead of loading data back also reduces the gain. Considering all the overhead, we set the $\beta$ to $0.1$ in our model.
\begin{equation}\label{eq:cacheGain}
%\scriptsize
\begin{aligned}
H_{one}  &= \beta \cdot N_{access} \cdot (t_{miss} - t_{hit}) \\
               &\approx \beta \cdot \alpha \cdot Activity \cdot period \cdot \frac{K_{f}(V - V_o)}{f_o^2}
\end{aligned}
\end{equation}
The $t_{miss} - t_{hit}$ is also inversely proportional to $f$ which has a linear relationship with $V$. Thus, the $H_{one}$ is also expanded in a Taylor series to find approximate linear relationship between $H_{one}$ and $V$. The constant $period$ indicates the time span of a period. The $N_{access}$ is expressed as $\alpha \cdot Activity$. $V_o$ is set as $1.1V$. In this case, the deviation of our fitting is less than $1\%$ when the voltage($V$) is in range $0.75V$ to $1.5V$.

Performance gain of all caches with different activity is shown in Equation~(\ref{eq:cacheGain2}). To make the linear relationship more clear, the $\bm{K}_{cache}$ is defined to indicate $(\beta \cdot \alpha \cdot period \cdot K_{f})/(f_o^2) \cdot \bm{A}_{cache}$.
\begin{equation}\label{eq:cacheGain2}
%\scriptsize
\begin{aligned}
H &= \frac{\beta \cdot \alpha \cdot period \cdot K_{f}}{f_o^2} \cdot \bm{A}_{cache} \cdot
            (\bm{V}_{cache} - V_o \cdot \bm{I}) \\
          &= \bm{K}_{cache} \cdot \bm{V}_{cache} + \bm{K}_{cache} \cdot (- V_o \cdot \bm{I}) \\
\end{aligned}
\end{equation}
\
\subsubsection{ILP Equation and Optimization}
\

Based on above simplification, The power management problem is converted to an integer linear programming (ILP) problem. SmartBoost would deliver this problem to a hardware ILP solver.
The restrictions and object function is shown in Equation~(\ref{eq:maximize}). The $V_{min}$ and $V_{max}$ is predetermined value to restrict the voltage range. In our experiment, the voltage range is $[0.75V,1.5V]$.
\begin{equation}\label{eq:maximize}
%\scriptsize
\begin{aligned}
maximize  \qquad   &(\bm{K}_{core} \cdot \bm{V}_{core} + \bm{K}_{cache} \cdot \bm{V}_{cache}) +  \\
                  %&(B_{gCore}+B_{gCache}) \\
                  &\bm{K}_{core} \cdot (- V_o \cdot \bm{I}) + \bm{K}_{cache} \cdot (- V_o \cdot \bm{I})\\
subject \quad to  \qquad  &\bm{V}^T\cdot\bm{K_p} +\bm{A}^T(B_{one}\cdot\bm{I}) \leq P_{spec} \\
and    \qquad         &\bm{V}_{i} \geq V_{min}, \quad \bm{V}_{i} \leq V_{max} \\
\end{aligned}
\end{equation}
To reduce the hardware overhead of ILP solver, SmartBoost could uses look-up table instead of hardware ILP solver. the value in table are calculated by ILP beforehead. Of course, when there are too many cores and caches in the chip, this method does not work well for it needs too much memory space.


\subsection{SmartBoost Power Migration}

Using an ILP solver, the SmartBoost power migration is implemented by the Algorithm~\ref{ag:ILP}. The SmartBoost work in parallel with the other components of the chip.
The algorithm can be divided into two parts.
(a) Line 1-2: The coefficients are initialized in factory, according to the configuration and attributes of the processor.
(b) Line 3-18: At the end of each sampling period, the activity of all components are predicted according to their history( SmartBoost is work parallel with the $k^{th}$ period, thus SmartBoost only have history record of $(k-1)^{th}$ and $(k-2)^{th}$ period), and the $\bm{K}_{p}$, $\bm{K}_{core}$, $\bm{K}_{cache}$, are calculated individually. At last the $\bm{V}$ is produced by the ILP solver or look-up table.

\begin{algorithm}
\caption{SmartBoost Power Migration Algorithm}
\label{ag:ILP}
\begin{algorithmic}[1]
\STATE Initialize $K_{one},B_{one},\alpha,t_{inst},K_{f},f_{o},V_{o},\beta,period$
\STATE $\bm{A_0} \leftarrow 1, \bm{V} \leftarrow 1.$
\FOR {each sampling period $k$}
    \STATE Initialize ${K}_{p}$, ${K}_{core}$, ${K}_{cache}$ to $0$.
    \STATE $\bm{A}_{k+1} \leftarrow $ActivityPredictor($\bm{A}_{k-1}, \bm{A}_{k-2}$)
    \STATE $\bm{K}_{p} \leftarrow K_{one}\cdot\bm{A}_{k+1}$
%	\STATE $\bm{B}_{p} \leftarrow \bm{A}_{k+1}^T(B_{pOne}\cdot\bm{I})$
	\FOR {each component $i$}
        \IF {component $i$ is a core}
            \STATE $\bm{K}_{core} \leftarrow (\alpha \cdot t_{inst} \cdot K_{f})/(f_o) \cdot \bm{A}_{k+1}[i]$
%            \STATE ${B}_{gCore} \leftarrow \bm{K}_{gCore} \cdot (- V_o \cdot \bm{I})$
        % \ENDIF
        \ELSIF {component $i$ is a cache}
            \STATE $\bm{K}_{cache} \leftarrow (\beta \cdot \alpha \cdot period \cdot K_{f})/(f_o^2) \cdot \bm{A}_{k+1}[i]$
%            \STATE ${B}_{gCache} \leftarrow \bm{K}_{gCache} \cdot (- V_o \cdot \bm{I})$
        \ENDIF
	\ENDFOR
%    \STATE ${B}_{g} \leftarrow {B}_{gCore} + {B}_{gCache}$
	\STATE $\bm{V} \leftarrow $ILPsolver(or look-up table)($\bm{K}_{p}$, $\bm{K}_{core}$, $\bm{K}_{cache}$)
\ENDFOR
\end{algorithmic}
\end{algorithm}

\subsection{Design Overhead}

To reduce overhead of activity predictor, SmartBoost uses the linear polynomial prediction method. According to the design compiler, the predictor of one component of the chip is not larger than $2000$ transistors. For the 4-core ARM chip used in our evaluation, it is divided into 9 components: 4 cores (includes L1 I/D caches), 4 private L2 caches and one shared L3 cache. The total hardware overhead of the predictor is less than $20\times10^{3}$ transistors. For each predictor of the component, a $2$-word space is used to record the history activity level, thus we need $18\times64 bit = 1152 b$ space.

In order to make the ILP solver fast enough for runtime decision making, a hardware ILP solver proposed by previous work \cite{simplex-hardware} is employed.
The scale of the ILP problem is estimated by the product of the number of variables and the number of constraint equations. The voltage vector is $9$ units in length. And the upper and lower bounds of the voltage contribute to $18$ constraints. In short, the ILP problem of a 4-core processor has a $9\times19$ scale. We use $2\times2$ block size and $12-bit$ word-length ILP hardware design proposed in previous work \cite{simplex-hardware}. According to the design compiler, the hardware overhead of ILP is about $2556\times10^{3}$ transistors.
There are a $18-Kbit$ RAM in each block, thus the ILP hardware solver need $72-Kbit$ RAM. The time cost of the ILP solver is less than $10us$.

{Add}The overhead of look-up table.
To avoid the cumbersome ILP hardware solver, SmartBoost could replace hardware solver by a look-up table. The index of table is activities of components in chip and the content is the voltages of components. For each component, there are $3$ activity levels and $3$ voltage levels. For a 9-length activity vector, $\log_{2}(3^{9}) \leq 16-bit$ memory space is need. Thus, SmartBoost with look-up table need $3^{9}\times16-bit = 314-Kbit$. According to CACTI 5.3, the look-up table would take $0.2917{mm}^2$ area. The time overhead would less than $1us$, it also need less than $1\times10^{3}$ transistors.

Evaluated at 22nm CMOS processing node, the detailed overhead analysis is shown in Table~\ref{tab:hardwareOverhead}. The span of period is $100us$. The overall time needed of SmartBoost is less than $10us$, thus SmartBoost could give the chip a voltage distribution of next period in time.

\begin{table}[thb]
%\vspace{-5pt}
\caption{The overhead of SmarBoost}\label{tab:hardwareOverhead}
%\algsetup{linenosize=\tiny}
\scriptsize
%\footnotesize
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
    & Transistor       &Memory&Area         &Power   & Time         \\
    &                  & (Kb) & (${mm}^2$)  & (mW)   &  (us)        \\    \hline
Predictor & $20\times10^{3}$    &$<$ $2$  &     $0.0025$   &$1.1$ &negligible           \\	\hline
ILP Solver  & $2556\times10^{3}$  & $72$           &     $0.3232$   &$136.9$ & $<$ 10       \\   \hline
look-up table  & $< 1\times10^{3}$ & $314$   &  $0.2917$    & $< 0.1$  & $< 1$        \\   \hline
Overall(ILP)   & $2576\times10^{3}$  & $<$ $74$ &     $0.3257$  &$138.0$  & $<$ 10       \\	\hline
Overall(table)   & $2557\times10^{3}$ & $< 316$& $0.2942$   &1.2 & $< 1$   \\	\hline
\end{tabular}
\end{table}

